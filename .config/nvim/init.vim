filetype plugin on " Load plugins for file type
filetype indent on " Load indent file for file type
syntax enable " Syntax highlighting

let mapleader = " "

colorscheme ron

set mouse=a " Enable the mouse in all modes
set number relativenumber " Relative line numbers except the current, which is absolute
set splitright splitbelow " Open new splits to the right or below
set guicursor=n-v-c:block-Cursor-blinkon1,i-ci:ver100-iCursor-blinkon1 " Blinking cursor
set lazyredraw " Don't update screen during macro etc.
set clipboard+=unnamedplus " Use clipboard for all operations
set hlsearch " Highlight all search matches
set ignorecase smartcase " Ignore case unless search term has capital letters
set guioptions=M " Don't source unnecessary file for GUI

autocmd BufWritePre * %s/\s\+$//e " Remove trailing white space
autocmd BufWritepre * %s/\n\+\%$//e " Remove trailing newlines
autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif " Return to last edit position when opening files

" Reload externally changed file
augroup checktime
    autocmd!
    autocmd BufEnter        * silent! checktime
    autocmd CursorHold      * silent! checktime
    autocmd CursorHoldI     * silent! checktime
    autocmd CursorMoved     * silent! checktime
    autocmd CursorMovedI    * silent! checktime
augroup END

set scrolloff=7 " Number of lines above and below the cursor

" Ignore compiled files
set wildignore=*.o,*~,*.pyc
if has("win16") || has("win32")
    set wildignore+=.git\*,.hg\*,.svn\*
else
    set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.DS_Store
endif

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4

" => Keybinds start """"""""""""""""""""""""""""""

" Move between windows
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Exit insert mode
inoremap jj <Esc>
" Exit insert mode when in a terminal
tnoremap <c-space> <c-\><c-n>

" Disable highlight when <leader><cr> is pressed
map <silent> <leader><cr> :nohlsearch<cr>

" Turn on spell checking
nnoremap <leader>bs :setlocal spell! spelllang=en_gb<cr>
" Save buffer
nmap <leader>bw :w!<cr>
" Close the current buffer
map <leader>bd :bdelete<cr>
" Move to next buffer
map <leader>l :bnext<cr>
" Move to previous buffer
map <leader>h :bprevious<cr>

" Useful mappings for managing tabs
map <leader>tn :tabnew<cr>
map <leader>to :tabonly<cr>
map <leader>td :tabclose<cr>
map <leader>tm :tabmove<cr>
map <leader>tl :tabnext<cr>
map <leader>th :tabprevious<cr>
" View all open buffers as tabs
map <leader>tb :tab sball<cr>

" => Keybinds end """"""""""""""""""""""""""""""

" Plugins
call plug#begin()
	"Status line
	Plug 'itchyny/lightline.vim'
	" Code commenting
	Plug 'tpope/vim-commentary'
	" Expand visually selected region
	Plug 'terryma/vim-expand-region'
	" Auto complete pairs of brackets or quotes
	Plug 'jiangmiao/auto-pairs'
	" Display git changes
	Plug 'airblade/vim-gitgutter'
	" Change surrounding character
	Plug 'tpope/vim-surround'

	" View undo tree
	Plug 'mbbill/undotree'
	nnoremap <silent> <leader>u :UndotreeToggle<cr>
call plug#end()
