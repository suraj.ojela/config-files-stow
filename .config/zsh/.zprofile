# Environmental variables
export FZF_DEFAULT_COMMAND='rg --files --hidden --no-ignore-vcs'
export FZF_DEFAULT_OPTS='--no-height --no-reverse'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_CTRL_T_OPTS="--select-1 --exit-0 --preview '(highlight -O ansi -l {} 2> /dev/null || cat {} || tree -C {}) 2> /dev/null | head -200'"
export FZF_CTRL_R_OPTS="--preview 'echo {}' --preview-window down:3:hidden:wrap --bind '?:toggle-preview'"
export FZF_ALT_C_COMMAND="find ! -path . -type d -maxdepth 10 -printf '%P\n'"
export FZF_ALT_C_OPTS="--select-1 --exit-0 --preview 'tree -C {} | head -200'"

export ALTERNATE_EDITOR=""
export EDITOR="emacsclient -t"
export VISUAL="nvim"
export TERMINAL=alacritty

export LESSHISTFILE="-"

export CALIBRE_USE_DARK_PALETTE="YES"

export SCRIPTS_DIR="$HOME"/.scripts
export STORAGE_DIR=/mnt/storage
export BACKUP_DIR="$STORAGE_DIR"/Backups
export SCREENSHOT_DIR="$HOME"/Screenshots
export SYNC_DIR="$HOME"/Sync
export KEEPASSXC_DIR="$HOME"/KeepassXC
export NOTES_DIR="$HOME"/Notes
export ZSHRC="$HOME"/.config/zsh/.zshrc