HISTSIZE=10000
SAVEHIST=10000
unsetopt beep
bindkey -v

zstyle :compinstall filename '~/.config/zsh/.zshrc'

autoload -Uz compinit
compinit

setopt HIST_IGNORE_ALL_DUPS # delete duplicates
setopt HIST_REDUCE_BLANKS # remove superfluous blanks from history items
setopt COMPLETE_ALIASES
setopt AUTO_CD
setopt APPEND_HISTORY
setopt HIST_SAVE_NO_DUPS
setopt HIST_FIND_NO_DUPS
setopt EXTENDED_HISTORY # save timestamp and duration of commands
setopt INC_APPEND_HISTORY # immediately add items to history
setopt PROMPT_SUBST # enable command subtitution in prompt

source /usr/share/fzf/shell/key-bindings.zsh

PLUGINSDIR="$HOME"/.config/zsh/plugins

source $PLUGINSDIR/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh
source $PLUGINSDIR/zsh-colored-man-pages/colored-man-pages.plugin.zsh
source $PLUGINSDIR/zsh-autopair/zsh-autopair.plugin.zsh
source $PLUGINSDIR/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh
source $PLUGINSDIR/zsh-interactive-cd/zsh-interactive-cd.plugin.zsh
source $PLUGINSDIR/zsh-system-clipboard/zsh-system-clipboard.plugin.zsh
source $PLUGINSDIR/zsh-vim-mode/zsh-vim-mode.plugin.zsh
source $PLUGINSDIR/zsh-command-time/command-time.plugin.zsh

export PROMPT='%F{yellow}%3~ %(?.%F{magenta}$.%F{red}$) '

alias rm='trash-put'
alias mv='mv -i'
alias cp='cp -i'
alias grep='grep --color=auto'
alias ls='eza -a'
alias l='eza -la'
alias v='nvim'
alias vsudo='sudo EDITOR="nvim" visudo'
alias less='nvim -R'
alias e='emacsclient --create-frame --alternate-editor="" --tty'
alias z='$SCRIPTS_DIR/zettelkasten-tag-search.sh'
alias sc='zsh -c $SCRIPTS_DIR/$(ls $SCRIPTS_DIR | fzf)'
alias yda='yt-dlp --continue --ignore-errors --extract-audio --audio-format mp3 -o "%(title)s.%(ext)s"'
alias rgain='mp3gain -r -c *.mp3'
alias ss='setsid -f'
alias blk='lsblk -o name,label,partlabel,size,mountpoint,uuid,partuuid'
alias sudo='doas'

dirModifiedUnixTime() {
    local dirModifiedTime=$(stat -c %y "$1" | sed 's/\..*//')
    local dirModifiedUnixTime=$(date -d "$dirModifiedTime" +%s)
    echo $dirModifiedUnixTime
}

borgLastBackupUnixTime() {
    local lastBackupTime=$(borg list --last 1 --format '{end}' "$1" | awk '{print $2,$3}')
    local lastBackupUnixTime=$(date -d "$lastBackupTime" +%s)
    echo "$lastBackupUnixTime"
}

# kill processes - list only the ones you can kill.
fkill() {
    local pid
    if [ "$UID" != "0" ]; then
        pid=$(ps -f -u $UID | sed 1d | fzf -m | awk '{print $2}')
    else
        pid=$(ps -ef | sed 1d | fzf -m | awk '{print $2}')
    fi

    if [ "x$pid" != "x" ]
    then
        echo $pid | xargs kill
    fi
}

extract() {$SCRIPTS_DIR/extract.sh "$1"}
wttr() {curl wttr.in/"$@"}

# expand aliases with a space
expand-alias() {
	zle _expand_alias
	zle self-insert
}
zle -N expand-alias
bindkey -M main ' ' expand-alias

# fzf history search
bindkey -M viins '^r' fzf-history-widget
bindkey -M vicmd '^r' fzf-history-widget

zstyle ':completion:*' rehash true # auto find new executables
zstyle ':completion:*' menu select # navigate suggestions with arrow keys

# use jj to exit insert mode
bindkey -M viins 'jj' vi-cmd-mode
export KEYTIMEOUT=100

# include hidden files in auto complete
_comp_options+=(globdots)

# edit command in editor
autoload -U edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line

# search previous commands using arrow keys
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey "^[[A" up-line-or-beginning-search # Up
bindkey "^[[B" down-line-or-beginning-search # Down

# TTY font
setfont iso02-12x22.psfu.gz 2>/dev/null