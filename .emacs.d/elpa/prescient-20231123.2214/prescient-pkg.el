(define-package "prescient" "20231123.2214" "Better sorting and filtering"
  '((emacs "25.1"))
  :commit "707c25c947a9f17a1d43f97b3b28aba91ec9addb" :authors
  '(("Radian LLC" . "contact+prescient@radian.codes"))
  :maintainers
  '(("Radian LLC" . "contact+prescient@radian.codes"))
  :maintainer
  '("Radian LLC" . "contact+prescient@radian.codes")
  :keywords
  '("extensions")
  :url "https://github.com/raxod502/prescient.el")
;; Local Variables:
;; no-byte-compile: t
;; End:
