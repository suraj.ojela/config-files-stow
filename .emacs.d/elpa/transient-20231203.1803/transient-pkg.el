(define-package "transient" "20231203.1803" "Transient commands"
  '((emacs "26.1")
    (compat "29.1.4.4")
    (seq "2.24"))
  :commit "413310cd049b176431527a1021e5b832e3ec6b5c" :authors
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainers
  '(("Jonas Bernoulli" . "jonas@bernoul.li"))
  :maintainer
  '("Jonas Bernoulli" . "jonas@bernoul.li")
  :keywords
  '("extensions")
  :url "https://github.com/magit/transient")
;; Local Variables:
;; no-byte-compile: t
;; End:
