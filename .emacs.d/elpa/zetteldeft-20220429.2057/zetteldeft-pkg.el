(define-package "zetteldeft" "20220429.2057" "Turn deft into a zettelkasten system"
  '((emacs "25.1")
    (deft "0.8")
    (ace-window "0.7.0"))
  :commit "86dd346be4bdddd6ac8d47503355fea350098271" :authors
  '(("EFLS <Elias Storms>"))
  :maintainer
  '("EFLS <Elias Storms>")
  :keywords
  '("deft" "zettelkasten" "zetteldeft" "wp" "files")
  :url "https://efls.github.io/zetteldeft/")
;; Local Variables:
;; no-byte-compile: t
;; End:
