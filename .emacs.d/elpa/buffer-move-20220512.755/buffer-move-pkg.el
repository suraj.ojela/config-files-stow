;;; Generated package description from buffer-move.el  -*- no-byte-compile: t -*-
(define-package "buffer-move" "20220512.755" "easily swap buffers" '((emacs "24.1")) :stars '(#("70" 0 2 (font-lock-face paradox-star-face))) :commit "e7800b3ab1bd76ee475ef35507ec51ecd5a3f065" :keywords '("convenience") :url "https://github.com/lukhas/buffer-move/")
