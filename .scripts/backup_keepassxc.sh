#!/usr/bin/zsh -l

source "$ZSHRC"

repo="$BACKUP_DIR"/borg-keepassxc
backupName=$(date --iso-8601=seconds | cut -d '+' -f 1)

dirToBackup="$KEEPASSXC_DIR"

backupTime=$(borgLastBackupUnixTime "$repo")
modifyTime=$(dirModifiedUnixTime "$dirToBackup")

if [ "$modifyTime" -gt "$backupTime" ]; then
    echo "Backing up KeepassXC..."
    borg create "$repo::$backupName" "$dirToBackup"
else
    echo "KeepassXC has not changed. Skipping backup."
fi