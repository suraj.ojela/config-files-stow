#!/usr/bin/zsh -l

source "$ZSHRC"

repo="/mnt/gdrive/"
backupName=$(date --iso-8601=seconds | cut -d '+' -f 1)

backupTime=$(borgLastBackupUnixTime "$repo")
backupsModifyTime=$(dirModifiedUnixTime "$STORAGE_DIR"/Backups/)
documentsModifyTime=$(dirModifiedUnixTime "$STORAGE_DIR"/Documents/)
installersModifyTime=$(dirModifiedUnixTime "$STORAGE_DIR"/Installers/)
musicModifyTime=$(dirModifiedUnixTime "$STORAGE_DIR"/Music/)
picturesModifyTime=$(dirModifiedUnixTime "$STORAGE_DIR"/Pictures/)
programmingModifyTime=$(dirModifiedUnixTime "$STORAGE_DIR"/Programming/)

times=($backupsModifyTime $documentsModifyTime $installersModifyTime $musicModifyTime $picturesModifyTime $programmingModifyTime)

lastModifyTime=$times[1]

for t in "${times[@]}"; do
    if [[ $t -gt $lastModifyTime ]]; then
	lastModifyTime=$t
    fi
done

if [ "$lastModifyTime" -gt "$backupTime" ]; then
    echo "Storage has changed. Creating backup..."
    time borg create --progress --stats --compression auto,lzma "$repo::$backupName" \
	 "$STORAGE_DIR"/Backups/ \
	 "$STORAGE_DIR"/Documents/ \
	 "$STORAGE_DIR"/Installers/ \
	 "$STORAGE_DIR"/Music/ \
	 "$STORAGE_DIR"/Pictures/ \
	 "$STORAGE_DIR"/Programming/
else
    echo "Storage has not changed. Skipping backup."
fi