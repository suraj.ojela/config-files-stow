#!/usr/bin/zsh -l

repo="$BACKUP_DIR/Fedora/borg-gnome-ext"
backupName=$(date --iso-8601=seconds | cut -d '+' -f 1)

currentList=$(gnome-extensions list)

previousArchive=$(borg list --last 1 --format '{archive}' "$repo")
previousList=$(borg extract "$repo::$previousArchive" --stdout)

if [ "$currentList" != "$previousList" ]; then
    echo "Gnome extensions list has changed. Creating backup..."
    echo "$currentList" | borg create "$repo::$backupName" -
else
    echo "Gnome extensions list has not changed. Skipping backup."
fi