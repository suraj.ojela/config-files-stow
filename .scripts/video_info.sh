#!/usr/bin/env sh

ffprobe -v error -select_streams v:0 -show_entries stream=width,height -show_entries format=filename,duration,size -sexagesimal -prefix -of default=noprint_wrappers=1 "$1"
