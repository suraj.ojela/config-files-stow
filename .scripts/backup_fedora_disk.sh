#!/usr/bin/env bash

if [[ $EUID -ne 0 ]]; then
    echo "This script must be run as root." >&2
    exit 1
fi

uuid="450E-A2B9"

get_block_device_name_by_uuid() {
    local uuid="$1"
    blkid_output=$(blkid -o export)
    while IFS='=' read -r key value; do
        if [ "$key" = "UUID" ] && [ "$value" = "$uuid" ]; then
            echo "$blkid_output" | grep -B 2 "$uuid" | cut -d'=' -f2 | sed -n '2p'
            return
        fi
    done <<< "$blkid_output"
}

get_parent_block_device_name_from_block_device_name() {
    part="$1"
    part=${part#/dev/}
    disk=$(readlink /sys/class/block/"$part")
    disk=${disk%/*}
    disk=/dev/${disk##*/}
    echo "$disk"
}


device_name_by_uuid=$(get_block_device_name_by_uuid "$uuid")
parent_device_name=$(get_parent_block_device_name_from_block_device_name "$device_name_by_uuid")
if [ -n "$device_name_by_uuid" ]; then
    echo "Block device to backup: $parent_device_name"
    time borg create --read-special --progress --compression auto,lzma /mnt/backup/borg-fedora-image::$(date --iso-8601) "$parent_device_name"
else
    echo "No block device found with UUID $uuid"
fi