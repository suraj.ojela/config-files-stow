#!/usr/bin/zsh -l

repo="$BACKUP_DIR/Fedora/borg-fedora-pkg"
backupName=$(date --iso-8601=seconds | cut -d '+' -f 1)

currentList=$(dnf repoquery --userinstalled)

previousArchive=$(borg list --last 1 --format '{archive}' "$repo")
previousList=$(borg extract "$repo::$previousArchive" --stdout)

if [ "$currentList" != "$previousList" ]; then
    echo "User-installed packages list has changed. Creating backup..."
    echo "$currentList" | borg create "$repo::$backupName" -
else
    echo "User-installed packages list has not changed. Skipping backup."
fi