#!/usr/bin/zsh -l

source "$ZSHRC"

repo="$BACKUP_DIR"/borg-sync
dirToBackup="$SYNC_DIR"
backupName=$(date --iso-8601=seconds | cut -d '+' -f 1)

backupTime=$(borgLastBackupUnixTime "$repo")
modifyTime=$(dirModifiedUnixTime "$dirToBackup")

if [ "$modifyTime" -gt "$backupTime" ]; then
    echo "Folder has changed. Creating backup..."
    borg create "$repo::$backupName" "$dirToBackup"
else
    echo "Folder has not changed. Skipping backup."
fi